import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  data:any;
  constructor(
    private http: HttpClient,
    ){
      this.http.get('http://127.0.0.1:8000/employee').subscribe(data => {
        this.data = data;
        console.log(this.data);
      }, error => console.error(error));
  }

  ngOnInit(): void {
  }

}
