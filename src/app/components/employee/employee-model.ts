export class EmployeeModel {
	constructor(
		public id: string= "",
		public first_name: string= "",
		public last_name: string= "",
		public email: string= "",
		public company_name: string= "",
		) { }
}
