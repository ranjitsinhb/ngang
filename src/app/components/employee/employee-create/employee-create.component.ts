import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeModel } from './../employee-model'
import { EmployeeService } from './../../../services/employee.service';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {

  serverMessage: any = [];
	employeeModel = new EmployeeModel();

  constructor(
    private router: Router,
    private _employeeService: EmployeeService,
  ) { }

  ngOnInit(): void {
  }

  saveEmployee() {
    // this._employeeService.saveEmployee(this.employeeModel).then((res: any) => {
    //   if (res.success) {
    //     this.router.navigate(['/employee']);
    //     // this._notificationsService.add(new Notification('success', res.message));
    //   } else {
    //     if(typeof res.message === 'object'){
    //       this.serverMessage = res.message;
    //     }else{
    //       // this._notificationsService.add(new Notification('error', res.message));
    //     }
    //   }
    // });
  }
}
