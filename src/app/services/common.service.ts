import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable()
export class CommonService {

	domainUrl: string = "";
	constructor(private _http: HttpClient) {
		this.domainUrl = window.location.origin;
	}

  	/**
     * @uses get user token from cookies
     * @author Ranjitsinh Bhalgariya
     * @return json
     */
     getFormToken() {
     	return new Promise((resolve, reject) => {
     		this._http.get(this.domainUrl + "/token/form-token").subscribe(
     			(res: any) => {
     				if(res.data != undefined && res.data != "") {
     					resolve(res.data);
     				} else {
     					resolve("");
     				}
     			},
     			error => {
     				reject(error);
     			}
     			)
     	})
     }

 }
