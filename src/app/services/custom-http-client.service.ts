import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { HttpClient } from '@angular/common/http';
import { CommonService } from './common.service';

@Injectable()
export class CustomHttpClientService {

	constructor(private _http: HttpClient, private _commonService: CommonService) { }

	get(url: string = "") {
		return new Promise((resolve, reject) => {
			this._http.get(url).subscribe((res: any) => {
				resolve(res);
			},error => {
				reject(error);
			})
		});
	}

	post(url:any, postData:any, is_image=false) {
		let formData: FormData = new FormData();
		return new Promise((resolve, reject) => {
			this._commonService.getFormToken().then((formToken: any)=> {
				postData._token = formToken;

				if(is_image) {
					for(var key in postData) {
						var value = postData[key];
						formData.append(key, value);
					}
				}else{
					formData = postData;
				}

				this._http.post(url, formData).subscribe((res: any) => {
					resolve(res);
				}, error => {
					reject(error);
				});
			});
		});
	}

}