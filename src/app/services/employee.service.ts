import { Injectable } from '@angular/core';
import { ConstantsService } from './../services/constants.service'
import { CustomHttpClientService } from './custom-http-client.service'

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private _constantsService : ConstantsService,
    private _customHttpClientService : CustomHttpClientService
    ) { }

  	saveEmployee(postData:any) {
		var url = this._constantsService.apiUrl + "/employee/create";
		return this._customHttpClientService.post(url, postData, true);
	}
	updateEmployee(postData:any) {
		var url = this._constantsService.apiUrl + "/employee/create";
		return this._customHttpClientService.post(url, postData, true);
	}
	getEmployeeDetailForUpdate(id:any) {
		var url = this._constantsService.apiUrl + "/employee/"+id+"/edit";
		return this._customHttpClientService.get(url);
	}
	employeeDelete(id:any) {
		var url = this._constantsService.apiUrl + "/employee/"+id;
		return this._customHttpClientService.get(url);
	}

}
